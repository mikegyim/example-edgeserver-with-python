This is an example edgeserver 

This is an example of a server implementation that includes authentication, encryption, rate limiting and load balancing. 

The client implementation includes encryption of the username, password, and message using the server's secret key and AES encryption.It t also sends the encrypted username and password to the server for authentication and receives an encrypted session key in response. Finally, it defines a send_message function that encrypts and sends a message to the server, and decrypts the response using the session key.  

Start the server first by running the following command:

python3 server.py

Start the client by running the following command:

python3 client.py


