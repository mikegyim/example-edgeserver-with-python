import socket
import threading
import hashlib
import time

# define the server address and port
server_address = ('localhost', 8000)

# create a TCP/IP socket
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# set socket options for reusing the address and enabling non-blocking mode
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.setblocking(False)

# bind the socket to the server address and port
server_socket.bind(server_address)

# listen for incoming connections
server_socket.listen(5)
print('Server listening on', server_address)

# define a dictionary to store user credentials
users = {
    'alice': 'password1',
    'bob': 'password2'
}

# define a dictionary to store session keys for authenticated clients
sessions = {}

# define a function to handle each client connection
def handle_client_connection(client_socket, client_address):
    print('Accepted connection from', client_address)

    try:
        # receive the encrypted message from the client
        encrypted_message = client_socket.recv(1024)

        # check if the client has an active session key
        if client_address in sessions:
            # decrypt the message using the session key
            session_key = sessions[client_address]
            decrypted_message = decrypt_message(encrypted_message, session_key)

            # process the decrypted message and generate a response
            response = process_message(decrypted_message)

            # encrypt the response using the session key
            encrypted_response = encrypt_message(response, session_key)

            # send the encrypted response to the client
            client_socket.send(encrypted_response)

        else:
            # receive the username and password from the client
            username, password = receive_credentials(client_socket)

            # check if the username and password are valid
            if authenticate_user(username, password):
                # generate a session key and store it in the sessions dictionary
                session_key = generate_session_key()
                sessions[client_address] = session_key

                # send the session key to the client
                encrypted_session_key = encrypt_message(session_key, password)
                client_socket.send(encrypted_session_key)

            else:
                # send an error message to the client
                client_socket.send('Invalid username or password'.encode())

    except socket.error:
        print('Error: Socket error occurred while receiving/sending data.')

    finally:
        # close the client socket
        client_socket.close()
        print('Closed connection with', client_address)

# receive the username and password from the client
def receive_credentials(client_socket):
    # receive the encrypted username and password from the client
    encrypted_username = client_socket.recv(1024)
    encrypted_password = client_socket.recv(1024)

    # decrypt the username and password using the server's secret key
    username = decrypt_message(encrypted_username, 'secret_key')
    password = decrypt_message(encrypted_password, 'secret_key')

    return username, password

# authenticate the user using the stored credentials
def authenticate_user(username, password):
    # check if the username exists in the users dictionary
    if username in users:
        # hash the password using SHA256 and compare it to the stored password hash
        password_hash = hashlib.sha256(password.encode()).hexdigest()
        stored_password_hash = hashlib.sha256(users[username].encode()).hexdigest()
        if password_hash == stored_password_hash:
            return True

    return False

# generate a session key using the current time as a seed
def generate_session_key():
    seed = str(time.time()).encode()
    session_key = hashlib.sha256(seed).hexdigest()
    return

