import socket
import hashlib
import time

# define the server address and port
server_address = ('localhost', 8000)

# create a TCP/IP socket
client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# connect to the server
client_socket.connect(server_address)

# define the username and password for authentication
username = 'alice'
password = 'password1'

# encrypt the username and password using the server's secret key
encrypted_username = encrypt_message(username, 'secret_key')
encrypted_password = encrypt_message(password, 'secret_key')

# send the encrypted username and password to the server
client_socket.send(encrypted_username)
client_socket.send(encrypted_password)

# receive the encrypted session key from the server
encrypted_session_key = client_socket.recv(1024)

# decrypt the session key using the password
session_key = decrypt_message(encrypted_session_key, password)

# define a function to send a message to the server
def send_message(message):
    # encrypt the message using the session key
    encrypted_message = encrypt_message(message, session_key)

    # send the encrypted message to the server
    client_socket.send(encrypted_message)

    # receive the encrypted response from the server
    encrypted_response = client_socket.recv(1024)

    # decrypt the response using the session key
    response = decrypt_message(encrypted_response, session_key)

    return response

# send a test message to the server
message = 'Hello, world!'
response = send_message(message)
print('Server response:', response)

# define a function to encrypt a message using AES encryption
def encrypt_message(message, key):
    # pad the message with spaces to make it a multiple of 16 bytes
    padded_message = message.ljust((len(message) // 16 + 1) * 16)

    # encrypt the padded message using AES encryption
    cipher = AES.new(key.encode(), AES.MODE_ECB)
    encrypted_message = cipher.encrypt(padded_message.encode())

    return encrypted_message

# define a function to decrypt a message using AES encryption
def decrypt_message(encrypted_message, key):
    # decrypt the message using AES encryption
    cipher = AES.new(key.encode(), AES.MODE_ECB)
    decrypted_message = cipher.decrypt(encrypted_message).decode().strip()

    return decrypted_message

